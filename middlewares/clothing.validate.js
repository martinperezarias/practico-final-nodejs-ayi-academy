const Joi = require("joi")

const validateIdParamClothing = async (req, res, next) => {
  const schema = Joi.object({
    _id: Joi.string().required(),
  }).required()

  try {
    await schema.validateAsync(req.params)
    return next()
  } catch (error) {
    return res.status(400).json({
      code: "VALIDATION-ERR",
      message: error.message,
      success: false,
      data: null
    })
  }
}


const validatePostClothing = async (req, res, next) => {
  const schema = Joi.object({
    type: Joi.string().valid( "BUZO", "REMERA", "CAMPERA", "PANTALON" ).required(),
    name: Joi.string().required(),
    sku: Joi.number().min(1).max(999999999).required(),
    quantity: Joi.number().min(1).max(100).required(),
    price: Joi.number().min(1).max(100000).required(),
    description: Joi.string().required(),
    featured: Joi.boolean()
  })

  try {
    await schema.validateAsync(req.body)
    return next()
  } catch (error) {
    return res.status(400).json({
      code: "VALIDATION-ERR",
      message: error.message,
      success: false,
      data: null
    })
  }
}


const validatePutClothing = async (req, res, next) => {
  const schema = Joi.object({
    type: Joi.string().valid( "BUZO", "REMERA", "CAMPERA", "PANTALON" ),
    name: Joi.string(),
    sku: Joi.number().min(1).max(99999999),
    quantity: Joi.number().min(1).max(100),
    price: Joi.number().min(1).max(100000),
    description: Joi.string(),
    featured: Joi.boolean()
  })

  try {
    await schema.validateAsync(req.body)
    return next()
  } catch (error) {
    return res.status(400).json({
      code: "VALIDATION-ERR",
      message: error.message,
      success: false,
      data: null
    })
  }
}

module.exports = {
  validateIdParamClothing,
  validatePostClothing,
  validatePutClothing
}