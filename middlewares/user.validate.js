const Joi = require("joi")

const validateIdParamUser = async (req, res, next) => {
  const schema = Joi.object({
    _id: Joi.string().required(),
  }).required()

  try {
    await schema.validateAsync(req.params)
    return next()
  } catch (error) {
    return res.status(400).json({
      code: "VALIDATION-ERR",
      message: error.message,
      success: false,
      data: null
    })
  }
}


const validatePostUser = async (req, res, next) => {
  const schema = Joi.object({
    name: Joi.string().required(),
    lastname: Joi.string().required(),
    password: Joi.string().required(),
    email: Joi.string().email().required()
  })

  try {
    await schema.validateAsync(req.body)
    return next()
  } catch (error) {
    return res.status(400).json({
      code: "VALIDATION-ERR",
      message: error.message,
      success: false,
      data: null
    })
  }
}


const validatePutUser = async (req, res, next) => {
  const schema = Joi.object({
    name: Joi.string(),
    lastname: Joi.string(),
    password: Joi.string(),
    email: Joi.string().email(),
    role: Joi.string().valid( "ADMIN", "EMPLOYEE", "USER" )
  })

  try {
    await schema.validateAsync(req.body)
    return next()
  } catch (error) {
    return res.status(400).json({
      code: "VALIDATION-ERR",
      message: error.message,
      success: false,
      data: null
    })
  }
}

module.exports = {
  validateIdParamUser,
  validatePostUser,
  validatePutUser
}