const User = require("../models/user")
const Joi = require("joi")
const jwt = require("jsonwebtoken")

const tokenValidate = async (req, res, next) => {
  
  if(req.url === "/auth/login" || req.url === "/users/register" || req.url === "/clothing/featured") return next()

  const authorization = req.headers.authorization
  
  if(!authorization) {
    return res.status(401).json({
      code: "AUTH-ERR",
      message: "AUTH TOKEN IS MISSING",
      success: false,
      data: null
    })
  }

  try {
    const token = authorization.split(" ")[1]

    const { _id } = jwt.verify(token, process.env.PRIVATE_KEY)

    const user = await User.findById({ _id })
    if(!user){
      return res.status(401).json({
        code: "NOT-FOUND",
        message: "USER DOES NOT EXIST IN DB",
        success: false,
        data: null
      })
    }

    req.user = user

    return next()

  } catch (error) {
    return res.status(400).json({
      code: "AUTH-ERR",
      message: error.message,
      success: false,
      data: null
    })
  }
}


const loginValidate = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string().email().required(),
    password: Joi.string().required(),
  })

  try {
    await schema.validateAsync(req.body)
    return next()
  } catch (error) {
    return res.status(400).json({
      code: "VALIDATION-ERR",
      message: error.message,
      success: false,
      data: null
    })
  }
}


const rolesValidate = (...roles) => {
  return async (req, res, next) => {
    if(!roles.includes(req.user.role)){
      return res.status(403).json({
        code: "AUTH-ERR",
        message: "RESTRICTED ACCESS",
        success: false,
        data: null
      })
    }
    return next()
  }
}


const adminValidate = async (req, res, next) => {

  try {
    const user = await User.findById(req.params._id)
    if(!user){
      return res.status(401).json({
        code: "NOT-FOUND",
        message: "USER DOESN'T EXIST IN DB",
        success: false,
        data: null
      })
    }
  
    if(String(req.user._id) === String(user._id) || String(req.user.role) === "ADMIN"){
      if(req.body.role && req.user.role !== "ADMIN"){
        return res.status(403).json({
          code: "VALIDATION-ERR",
          message: "YOU DON'T HAVE PERMISSION TO DO THAT",
          success: false,
          data: null
        })
      }
      return next()
    }else{
      return res.status(403).json({
        code: "AUTH-ERR",
        message: "RESTRICTED ACCESS",
        success: false,
        data: null
      })
    }  
    
  } catch (error) {
    return res.status(400).json({
      code: "VALIDATION-ERR",
      message: error.message,
      success: false,
      data: null
    })
  }
  
}


module.exports = {
  tokenValidate,
  loginValidate,
  rolesValidate,
  adminValidate
}
