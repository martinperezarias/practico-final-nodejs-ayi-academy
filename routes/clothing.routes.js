const express = require('express');
const router = express.Router();

const { 
  rolesValidate
} = require("../middlewares/auth.validate")

const {
  validateIdParamClothing,
  validatePostClothing,
  validatePutClothing
} = require('../middlewares/clothing.validate')

const {
  getAllClothes,
  getClothing,
  getFeatured,
  postClothing,
  putClothing,
  deleteClothing,
  uploadProductImgFile
} = require('../controllers/clothing.controller')

router.get('/', getAllClothes);
router.get('/featured', getFeatured);
router.get('/:_id', validateIdParamClothing, getClothing);
router.post('/', [rolesValidate("ADMIN", "EMPLOYEE"), validatePostClothing], postClothing);
router.put('/:_id', [rolesValidate("ADMIN", "EMPLOYEE"), validatePutClothing, validateIdParamClothing], putClothing);
router.delete('/:_id', [rolesValidate("ADMIN", "EMPLOYEE"), validateIdParamClothing], deleteClothing);
router.post('/upload/:_id', [rolesValidate("ADMIN", "EMPLOYEE"), validateIdParamClothing], uploadProductImgFile);

module.exports = router;