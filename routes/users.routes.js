const express = require('express');
const router = express.Router();

const { adminValidate } = require("../middlewares/auth.validate")

const {
  validateIdParamUser,
  validatePostUser,
  validatePutUser
} = require('../middlewares/user.validate')

const {
  getUser,
  postUser,
  putUser,
  deleteUser
} = require('../controllers/user.controller')

router.get('/:_id', [adminValidate, validateIdParamUser], getUser);
router.post('/register', validatePostUser, postUser);
router.put('/:_id', [adminValidate, validatePutUser, validateIdParamUser], putUser);
router.delete('/:_id', [adminValidate, validateIdParamUser], deleteUser);

module.exports = router;
