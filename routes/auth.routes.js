const express = require('express')
const router = express.Router()

const login = require("../controllers/auth.controller")

const { loginValidate } = require("../middlewares/auth.validate")

router.post('/login', loginValidate, login);

module.exports = router