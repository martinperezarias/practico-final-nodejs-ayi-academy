const mongoose = require("mongoose")

const dbConnection = async () => {
  try {
    await mongoose.connect(process.env.CONNECTION_DB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false,
    })
    console.log("DB Connected")
  } catch (error) {
    console.log(error)
  }
}

module.exports = dbConnection