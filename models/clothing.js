const { Schema, model } = require("mongoose")

const ClothingSchema = Schema(
  {
    type: {
      type: String,
      required: true,
      enum: [ "BUZO", "REMERA", "CAMPERA", "PANTALON" ],
    },
    name: {
      type: String,
      required: true
    },
    sku: {
      type: Number,
      required: true
    },
    quantity: {
      type: Number,
      required: true
    },
    price: {
      type: Number,
      required: true
    },
    description: {
      type: String,
      required: true
    },
    featured: {
      type: Boolean,
      default: false
    }
  },
  {
    timestamps: true
  }
)

ClothingSchema.methods.toJSON = function() {
  const { createdAt, updatedAt, __v, ...clothing } = this.toObject()
  return clothing
}

module.exports = model("Clothing", ClothingSchema)