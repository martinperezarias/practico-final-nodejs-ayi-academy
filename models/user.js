const { Schema, model } = require("mongoose")

const UserSchema = Schema(
  {
    name: {
      type: String,
      required: true
    },
    lastname: {
      type: String,
      required: true
    },
    password: {
      type: String,
      required: true
    },
    email: {
      type: String,
      required: true,
      unique: true
    },
    role: {
      type: String,
      required: true,
      default: "ADMIN",
      enum: [ "ADMIN", "EMPLOYEE", "USER" ]
    }
  },
  {
    timestamps: true
  }
)

UserSchema.methods.toJSON = function() {
  const { password, createdAt, updatedAt, __v, ...user } = this.toObject()
  return user
}

module.exports = model("User", UserSchema)