require("dotenv").config()
const express = require("express")
const morgan = require("morgan")
const cors = require("cors")
const DBConnection = require("./configs/mongodb")

const authRouter = require('./routes/auth.routes')
const usersRouter = require('./routes/users.routes')
const clothingRouter = require('./routes/clothing.routes')

const { tokenValidate } = require("./middlewares/auth.validate")

const app = express()

DBConnection()

app.use(express.text())
app.use(express.json())
app.use(morgan('tiny'))
app.use(cors())

app.use(tokenValidate)

app.use('/auth', authRouter);
app.use('/users', usersRouter);
app.use('/clothing', clothingRouter);


app.use((req, res, next) =>{
  res.status(404).json({
    code: "NOT-FOUND",
    message: null,
    success: false,
    data: null
  })
})

app.listen((process.env.PORT), () => {
  console.log(`Server running in PORT ${process.env.PORT}`)
})
