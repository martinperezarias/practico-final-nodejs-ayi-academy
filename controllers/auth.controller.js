const User = require("../models/user")
const jwt = require("jsonwebtoken")
const bcryptjs = require("bcryptjs")

const login = async (req, res) => {

  const { email, password } = req.body
  
  try {
    const user = await User.findOne({ email })

    if(!user || !bcryptjs.compareSync(password, user.password)){
      return res.status(401).json({
        code: "AUTH-ERR",
        message: "INVALID EMAIL OR PASSWORD",
        success: false,
        data: null
      })
    }

    const token = jwt.sign({ _id: user.id }, process.env.PRIVATE_KEY, { expiresIn: "3h" })

    return res.status(200).json({
      code: "OK",
      message: null,
      success: true,
      data: {
        user,
        token
      }
    })
  } catch (error) {
    return res.status(500).json({
      code: "ERR",
      message: error.message,
      success: false,
      data: null
    })
  }
}

module.exports = login