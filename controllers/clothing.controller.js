const Clothing = require("../models/clothing")
const uploadProductImg = require("../helpers/uploadProductImg")

const getAllClothes = async (req, res) => {
  
  const page = Number(req.query.page)
  const limit = Number(req.query.limit)
  const skipIndex = (page - 1)*limit

  try {
    const count = await Clothing.countDocuments()
    const results = await Clothing.find()
    .sort({ _id: 1 })
    .limit(limit)
    .skip(skipIndex)
    return res.status(200).json({
      code: "OK",
      message: null,  
      success: true,
      data: {
        count,
        results
      }
    })
  } catch (error) {
    return res.status(400).json({
      code: "ERR",
      message: error.message,
      success: false,
      data: null
    })
  }
}

const getClothing = async (req, res) => {
  
  try {
    const clothing = await Clothing.findById(req.params._id)
    if(!clothing){
      return res.status(404).json({
        code: "NOT-FOUND",
        message: null,
        success: false,
        data: null
      })
    }
    return res.status(200).json({
      code: "OK",
      message: null,
      success: true,
      data: clothing
    })

  } catch (error) {
    return res.status(400).json({
      code: "ERR",
      message: error.message,
      success: false,
      data: null
    })
  }
}

const getFeatured = async (req, res) => {
  
  try {
    const results = await Clothing.find({ featured: true })
    const count = await Clothing.countDocuments({ featured: true })
    if(!results){
      return res.status(404).json({
        code: "NOT-FOUND",
        message: null,
        success: false,
        data: null
      })
    }
    return res.status(200).json({
      code: "OK",
      message: null,  
      success: true,
      data: {
        count,
        results
      }
    })
  } catch (error) {
    return res.status(400).json({
      code: "ERR",
      message: error.message,
      success: false,
      data: null
    })
  }
}

const postClothing = async (req, res) => {
  
  try {
    const clothing = await Clothing.create(req.body)
    return res.status(201).json({
      code: "CLOTHES-CREATED",
      message: null,
      success: true,
      data: clothing
    })
  } catch (error) {
    return res.status(500).json({
      code: "ERR",
      message: error.message,
      success: false,
      data: null
    })
  }

}

const putClothing = async (req, res) => {
  
  try {
    const clothing = await Clothing.findOneAndUpdate({ _id: req.params._id }, { ...req.body }, { new: true })
    if(!clothing){
      return res.status(404).json({
        code: "NOT-FOUND",
        message: null,
        success: false,
        data: null
      })
    }
    return res.status(200).json({
      code: "CLOTHES-UPDATED",
      message: null,
      success: true,
      data: clothing
    })

  } catch (error) {
    return res.status(500).json({
      code: "ERR",
      message: error.message,
      success: false,
      data: null
    })
  }

}

const deleteClothing = async (req, res) => {

  try {
    const clothing = await Clothing.findById(req.params._id)
    if(!clothing){
      return res.status(404).json({
        code: "NOT-FOUND",
        message: "CLOTHES DOESN'T EXIST IN DB",
        success: false,
        data: null
      })
    }

    await Clothing.deleteOne({ _id: req.params._id })
    return res.status(200).json({
      code: "CLOTHES-DELETED",
      message: null,
      success: true,
      data: null
    })
  } catch (error) {
    return res.status(400).json({
      code: "ERR",
      message: error.message,
      success: false,
      data: null
    })
  }
}

const uploadProductImgFile = async (req, res) => {
  
  uploadProductImg(req, res, (error) => {

    if (error) {
      return res.status(400).json({
        code: "FILE-ERR",
        message: error.message,
        success: false,
        data: null
      })
    } else if (!req.file) {
      return res.status(400).json({
        code: "FILE-ERR",
        message: "YOU NEED TO SELECT A FILE",
        success: false,
        data: null
      })
    }

    return res.status(200).json({
      code: "IMG-LOADED",
      message: null,
      success: true,
      data: req.file
    })
  })
}

module.exports = {
  getAllClothes,
  getClothing,
  getFeatured,
  getClothing,
  postClothing,
  putClothing,
  deleteClothing,
  uploadProductImgFile
}