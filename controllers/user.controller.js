const User = require("../models/user")
const bcryptjs = require("bcryptjs")

const getUser = async (req, res) => {
  
  try {
    const user = await User.findById(req.params._id)
    if(!user){
      return res.status(404).json({
        code: "NOT-FOUND",
        message: null,
        success: false,
        data: null
      })
    }
    return res.status(200).json({
      code: "OK",
      message: null,
      success: true,
      data: user
    })

  } catch (error) {
    return res.status(400).json({
      code: "ERR",
      message: error.message,
      success: false,
      data: null
    })
  }
}

const postUser = async (req, res) => {
  
  const { password, ...resto } = req.body

  resto.password = bcryptjs.hashSync(password, 10);

  try {
    const user = await User.create(resto)
    return res.status(201).json({
      code: "USER-CREATED",
      message: null,
      success: true,
      data: user
    })
  } catch (error) {
    return res.status(500).json({
      code: "ERR",
      message: error.message,
      success: false,
      data: null
    })
  }

}

const putUser = async (req, res) => {

  const { password, ...resto } = req.body

  if(password){
    resto.password = bcryptjs.hashSync(password, 10)    
  }

  try {
    const user = await User.findOneAndUpdate(
      { _id: req.params._id }, 
      { ...resto }, 
      { new: true }
    )  
    if(!user){
      return res.status(404).json({
        code: "NOT-FOUND",
        message: null,
        success: false,
        data: null
      })
    }
    return res.status(200).json({
      code: "USER-UPDATED",
      message: null,
      success: true,
      data: user
    })

  } catch (error) {
    return res.status(500).json({
      code: "ERR",
      message: error.message,
      success: false,
      data: null
    })
  }

}

const deleteUser = async (req, res) => {
  
  try {
    const user = await User.findById(req.params._id)
    if(!user){
      return res.status(404).json({
        code: "NOT-FOUND",
        message: "USER DOESN'T EXIST IN DB",
        success: false,
        data: null
      })
    }

    await User.deleteOne({ _id: req.params._id })
    return res.status(200).json({
      code: "USER-DELETED",
      message: null,
      success: true,
      data: null
    })

  } catch (error) {
    return res.status(400).json({
      code: "ERR",
      message: error.message,
      success: false,
      data: null
    })
  }
}

module.exports = {
  getUser,
  getUser,
  postUser,
  putUser,
  deleteUser
}