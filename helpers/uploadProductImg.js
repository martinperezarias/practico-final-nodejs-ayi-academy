const multer = require("multer")
const path = require("path")

const storage = multer.diskStorage({
  destination: (req, file, callback) => {
    callback(null, "./uploads")
  },
  filename: (req, file, callback) => {
    callback(null, `${file.fieldname}-${req.params._id}${path.extname(file.originalname)}`)
  }
})

const fileFilter = (req, file, callback) => {
  if (file.mimetype !== "image/png"){
    return callback(new Error("Invalid file format. Only '.png' files are allowed"))
  }
  return callback(null, true)
}

const limits = { fileSize: 10000000 }

const uploadProductImg = multer({ 
  storage,
  limits,
  fileFilter
}).single("product-img");

module.exports = uploadProductImg